package com.kosign.fcmdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridViewAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private String[] numberWords;
    private int[] numberImages;

    public GridViewAdapter(Context context,String[] numberWords,int[] numberImages){
        this.context = context;
        this.numberWords = numberWords;
        this.numberImages = numberImages;
    }

    @Override
    public int getCount() {
        return numberWords.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.row_items,null);
        }
        ImageView imageView = convertView.findViewById(R.id.img_numberImage);
        TextView textView = convertView.findViewById(R.id.tv_numWords);
        //set data
        imageView.setImageResource(numberImages[position]);
        textView.setText(numberWords[position]);
        return convertView;
    }
}
