package com.kosign.fcmdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import io.fabric.sdk.android.Fabric;


public class MainActivity extends AppCompatActivity {

    GridView gridView;
    String[] numberWords = {"Activity","WebView"};
    int[] numberImages = {R.drawable.ic_shopping,R.drawable.ic_websim};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ReplaceFont.replaceDefaultFont(this,"DefaultFont","simm.ttf");

        /**
         * How to test Ours App Crash
         * */
        /*Fabric.with(this, new Crashlytics());
        Crashlytics.getInstance().crash(); // Force a crash*/


        //token for sending to specific device
        //for sending to all device using own server code subscribe your app to one topic
        Log.d(">>>> ",""+ FirebaseInstanceId.getInstance().getToken());
        FirebaseMessaging.getInstance().subscribeToTopic("allDevices");

        gridView = findViewById(R.id.grid_view);
        GridViewAdapter mAdapter = new GridViewAdapter(MainActivity.this,numberWords,numberImages);
        gridView.setAdapter(mAdapter);



        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    Intent intent = new Intent(MainActivity.this,ShoppingActivity.class);
                    startActivity(intent);
                }else if (position ==1){

                }

            }
        });


    }





    /*// for request Message

    https://fcm.googleapis.com/fcm/send ==> POST
    Content-Type ==> application/json
    Authorization ==> key= ours server key that get from firebase console

    body
    {
        "registration_ids":["ePHcYV0YiuU:APA91bH3F5FiumFtRUG7vtqH3ExawuuQ4s01Y5XP0kAvdcmnyOMyJHSkAOWgDG90E1RitzakUM5yQFXkQHneTyaF-ye-vWUE2uBinfXcbcRt7iZOdAtsw5QnR9NX6Jq3ZF3bWWeJzogA"],
        "data": {
        "title" : "Win$1000",
        "message" : "새로운 메시지이 도착했습니다"
        }
    }*/
}
