package com.kosign.fcmdemo;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.lang.reflect.Field;

public class ReplaceFont {
    public static void replaceDefaultFont(Context context, String nameOfFontBeingReplaced, String nameOfFontInAssert){
        Typeface customFontTypeFace = Typeface.createFromAsset(context.getAssets(),nameOfFontInAssert);
        replaceFont(nameOfFontBeingReplaced,customFontTypeFace);
    }

    private static void replaceFont(String nameOfFontBeingReplaced, Typeface customFontTypeFace) {
        try{
            Field mField = Typeface.class.getDeclaredField(nameOfFontBeingReplaced);
            mField.setAccessible(true);
            mField.set(null,customFontTypeFace);
            Log.d(">>>", "1");
        }catch (NoSuchFieldException e){
            e.printStackTrace();
            Log.d(">>>", "2");
        } catch (IllegalAccessException e) {
            Log.d(">>>", "3");
            e.printStackTrace();
        }
    }
}
